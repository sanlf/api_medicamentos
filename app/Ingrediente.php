<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
    protected $fillable = [
        'nombre',
        'cantidad',
        'cas',
    ];

    protected $hidden = [
        'pivot',
        'created_at',
        'updated_at',
    ];

    public function medicamentos()
    {
        return $this->belongsToMany(Medicamento::class, 'ingrediente_medicamento');
    }
}

