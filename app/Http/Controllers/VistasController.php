<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Medicamento;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class VistasController extends Controller
{

    public function tabla_medicamentos(){
        $medicamentos = Medicamento::paginate();
        return view('tabla_medicamentos')->with('medicamentos', $medicamentos);
    }
}

