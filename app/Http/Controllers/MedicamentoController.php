<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medicamento;

class MedicamentoController extends Controller
{
    public function index(){
        //Descomentar la siguiente linea para usar paginacion
        //return Medicamento::jsonPaginate();

        return Medicamento::with('ingredientes')->get();
    }

    public function show(Medicamento $medicamento){
        return $medicamento;
    }

    public function store(Request $request){
        $medicamento = Medicamento::create($request->all());

        return response()->json($medicamento, 201);
    }

    public function update(Request $request, Medicamento $medicamento){
        $medicamento->update($request->all());

        return response()->json($medicamento, 200);
    }

    public function delete(Request $request, Medicamento $medicamento){
        $medicamento->delete();

        return response()->json(null, 204);
    }
}

