<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicamento extends Model
{
    protected $fillable = [
        'partida',
        'codigo_interno',
        'descripcion',
    ];

    protected $hidden = [
        'pivot',
        'created_at',
        'updated_at',
    ];

    public function resolveRouteBinding($value)
    {
        return $this->where('id', $value)->with('ingredientes')->first() ?? abort(404);
    }

    public function ingredientes()
    {
        return $this->belongsToMany(Ingrediente::class, 'ingrediente_medicamento');
    }
}

