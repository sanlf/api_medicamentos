#!/bin/bash

echo -e "Programa para mostrar el uso de la API de medicamentos por medio de cURL\n";

echo -e "Borra la base de datos para comenzar desde 0";
php artisan migrate:fresh --seed;

#########REGISTRO#####################
echo -e "\n#############################\n"
echo -e "Registra un nuevo usuario con:
        name: nombre
        email: mi_correo@gmail.com
        password: contra123\n";

echo -e "Usando el comando de curl:
    curl -X POST http://localhost:8000/api/register
        -H \"Accept: application/json\"
        -H \"Content-Type: application/json\"
        -d '{\"name\": \"nombre\", \"email\": \"mi_correo@gmail.com\", \"password\": \"contra123\", \"password_confirmation\": \"contra123\"}'\n";

curl -X POST http://localhost:8000/api/register \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d '{"name": "nombre", "email": "mi_correo@gmail.com", "password": "contra123", "password_confirmation": "contra123"}';

if [ -z "$pausa" ]; then
    echo ""
    read -p "Presione [Enter] para continuar...";
fi

#########LOGIN########################
echo -e "\n#############################\n"
echo -e "\nHace login con la informacion del usuario recien creado:\n"

echo -e "Usando el comando de curl:
    curl -X POST localhost:8000/api/login
      -H \"Accept: application/json\"
      -H \"Content-type: application/json\"
      -d \"{\"email\": \"mi_correo@gmail.com\", \"password\": \"contra123\" }\n"


json=$(curl -X POST localhost:8000/api/login \
      -H "Accept: application/json" \
      -H "Content-type: application/json" \
      -d "{\"email\": \"mi_correo@gmail.com\", \"password\": \"contra123\" }"
);
#obtiene el api_token y le quita las comillas (")
api_token=$(echo ${json} | jq '.api_token' | sed 's/"//g');

echo -e "El API token es: $api_token\n";

if [ -z ${pausa+x} ]; then
    echo ""
    read -p "Presione [Enter] para continuar...";
fi


#########OBTENER TODOS MEDICAMENTOS###
echo -e "\n#############################\n"
echo -e "---Obteniendo todos los medicamentos---\n";

echo -e "Con el comando de curl:\n";

echo -e "curl -X GET localhost:8000/api/medicamentos
      -H \"Accept: application/json\"
      -H \"Content-type: application/json\"
      -H \"Authorization: Bearer $api_token\"\n";

curl -X GET localhost:8000/api/medicamentos \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -H "Authorization: Bearer $api_token" | jq -c '.[]';

if [ -z ${pausa+x} ]; then
    echo ""
    read -p "Presione [Enter] para continuar...";
fi


#########OBTENER UN MEDICAMENTOS###
echo -e "\n#############################\n"
echo -e "---Obteniendo un medicamento (el tercero)---\n";

echo -e "Con el comando de curl:\n";

echo -e "curl -X GET localhost:8000/api/medicamentos/3
      -H \"Accept: application/json\"
      -H \"Content-type: application/json\"
      -H \"Authorization: Bearer $api_token\"\n";

curl -X GET localhost:8000/api/medicamentos/3 \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -H "Authorization: Bearer $api_token" | jq -c '.';

if [ -z ${pausa+x} ]; then
    echo ""
    read -p "Presione [Enter] para continuar...";
fi


#########Actualiza UN MEDICAMENTOS###
echo -e "\n#############################\n"
echo -e "---Actualiza un medicamento (el segundo)---\n";

echo -e "Con el comando de curl:\n";

echo -e "curl -X PUT localhost:8000/api/medicamentos/2
      -H \"Accept: application/json\"
      -H \"Content-type: application/json\"
      -H \"Authorization: Bearer $api_token\"
      -d '{\"partida\": \"nueva_partida\",
            \"codigo_interno\": \"nuevo_codigo_interno\",
            \"descripcion\": \"nueva_descripcion\"}'\n";

curl -X PUT localhost:8000/api/medicamentos/2 \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -H "Authorization: Bearer $api_token" \
  -d '{"partida": "nueva_partida", "codigo_interno": "nuevo_codigo_interno", "descripcion": "nueva_descripcion"}';

echo -e "Obteniendo el segundo medicamento...\n";
curl -X GET localhost:8000/api/medicamentos/2 \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -H "Authorization: Bearer $api_token" | jq -c '.';

if [ -z ${pausa+x} ]; then
    echo ""
    read -p "Presione [Enter] para continuar...";
fi

#########BORRA UN MEDICAMENTOS###
echo -e "\n#############################\n"
echo -e "---Borra un medicamento (el segundo)---\n";

echo -e "Con el comando de curl:\n";

echo -e "curl -X DELETE localhost:8000/api/medicamentos/2
      -H \"Accept: application/json\"
      -H \"Content-type: application/json\"
      -H \"Authorization: Bearer $api_token\"\n";

curl -X DELETE localhost:8000/api/medicamentos/2 \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -H "Authorization: Bearer $api_token";

echo -e "Obteniendo todos los medicamentos...\n";
curl -X GET localhost:8000/api/medicamentos \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -H "Authorization: Bearer $api_token" | jq -c '.[]';

if [ -z ${pausa+x} ]; then
    echo ""
    read -p "Presione [Enter] para continuar...";
fi

#########LOGOUT################
echo -e "\n#############################\n"
echo -e "---Realiza logout---\n";

echo -e "Con el comando de curl:\n";

echo -e "curl -X POST localhost:8000/api/logout
  -H \"Accept: application/json\"
  -H \"Content-type: application/json\"
  -H \"Authorization: Bearer $api_token\"\n";

curl -X POST localhost:8000/api/logout \
  -H "Accept: application/json" \
  -H "Content-type: application/json" \
  -H "Authorization: Bearer $api_token";

echo -e "\nFin del programa"

