<?php

use Illuminate\Http\Request;
use App\Article;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Auth::guard('api')->user(); // instance of the logged user
Auth::guard('api')->check(); // if a user is authenticated
Auth::guard('api')->id(); // the id of the authenticated user


Route::group(['middleware' => 'auth:api'], function(){
    Route::get    ( 'medicamentos'               , 'MedicamentoController@index');
    Route::get    ( 'medicamentos/{medicamento}' , 'MedicamentoController@show');
    Route::post   ( 'medicamentos'               , 'MedicamentoController@store');
    Route::put    ( 'medicamentos/{medicamento}' , 'MedicamentoController@update');
    Route::delete ( 'medicamentos/{medicamento}' , 'MedicamentoController@delete');
});

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

