<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Medicamento;
use Faker\Generator as Faker;

$factory->define(Medicamento::class, function (Faker $faker) {
    return [
        'partida'        => $faker->randomNumber(5),
        'codigo_interno' => $faker->randomNumber(5),
        'descripcion'    => $faker->paragraph,
    ];
});
