<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Ingrediente;
use Faker\Generator as Faker;

$factory->define(Ingrediente::class, function (Faker $faker) {
    return [
        'nombre'   => $faker->word,
        'cantidad' => $faker->randomNumber(2),
        'cas'      => $faker->randomNumber(7),
    ];
});

