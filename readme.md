# Notas

## Manual de instalacion

Laravel es un framework muy utilizado y hay muchas guias dedicadas a aprender como se usa.
Nombrare los pasos que considero que son suficientes pero puedo resultar ambiguo. En caso de cualquier duda,
enlazo varias paginas que explican como instalar aplicaciones de laravel al final de la seccion de instalacion.


### Requisitos

- Apache
- MySQL
- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- Composer, manejador de paquetes de laravel
- El servidor web debe estar configurado para que apunte a la carpeta public del proyecto 

En el archivo .env dentro de la raiz del proyecto, las variables:

- APP_URL
- DB_CONNECTION
- DB_HOST
- DB_PORT
- DB_DATABASE
- DB_USERNAME
- DB_PASSWORD

deben coincidir con las credenciales de la base de datos a utilizar y la direccion URL de la aplicacion

###

Hay que correr los comandos:

    composer update

    composer install

    composer dump-autoload

    php artisan key:generate

    php artisan migrate:fresh --seed

Y finalmente corre el comando:

```
php artisan up
```

Con lo cual el servidor deberia estar corriendo la aplicacion de laravel y la API estar disponible.

### Paginas de instalacion

- https://laraveldaily.com/how-to-deploy-laravel-projects-to-live-server-the-ultimate-guide/
- https://laravel.com/docs/5.8/deployment


## Manual de uso

La API tiene las funciones mas basicas que se esperan:

- Obtener todos los medicamentos disponibles.
- Obtener un medicamento en particular dado un identificador.
- Actualiza la informacion de un medicamento dado un identificador.
- Borra un medicamento dado un identificador.

La informacion disponible de los medicamentos es:

- partida 
- codigo_interno 
- descripcion 
- lista de ingredientes (los cuales a su vez tienen otros atributos)
    - nombre
    - cantidad (manejada en porcentaje y hay casos en donde se maneja como rango, por ejemplo 60-70)
    - cas


Para poder usar la API, se tiene que registrar como usuario y despues hacer login.
Una vez hecho el login, se pueden realizar cualquiera de las funciones basicas que la API provee.
Finalmente, el logout cuando se obtuvo la informacion requerida de la API.

Asumiendo que la direccion base donde se encuentra la API es "localhost:8000", usando curl se usaria de la siguiente manera:

### Registro

    curl -X POST http://localhost:8000/api/register \
        -H "Accept: application/json" \
        -H "Content-Type: application/json" \
        -d '{"name": "nombre", "email": "mi_correo@gmail.com", "password": "contra123", "password_confirmation": "contra123"}'

### Login

    curl -X POST localhost:8000/api/login \
      -H "Accept: application/json" \
      -H "Content-type: application/json" \
      -d "{\"email\": \"mi_correo@gmail.com\", \"password\": \"contra123\" }"

El login regresa un Token de API, necesario para realizar cualquier accion.

### Obtener todos los medicamentos

    curl -X GET localhost:8000/api/medicamentos \
      -H "Accept: application/json" \
      -H "Content-type: application/json" \
      -H "Authorization: Bearer TOKEN_API"

### Obtener un medicamento

Obtiene el medicamento con id ID

    curl -X GET localhost:8000/api/medicamentos/ID \
      -H "Accept: application/json" \
      -H "Content-type: application/json" \
      -H "Authorization: Bearer TOKEN_API"

### Actualizar un medicamento

Actualiza el medicamento con id ID

    curl -X PUT localhost:8000/api/medicamentos/ID \
      -H "Accept: application/json" \
      -H "Content-type: application/json" \
      -H "Authorization: Bearer TOKEN_API" \
      -d '{"partida": "nueva_partida", "codigo_interno": "nuevo_codigo_interno", "descripcion": "nueva_descripcion"}'

### Borrar un medicamento

Borra el medicamento con id ID

    curl -X DELETE localhost:8000/api/medicamentos/ID \
      -H "Accept: application/json" \
      -H "Content-type: application/json" \
      -H "Authorization: Bearer TOKEN_API"

### Logout

    curl -X POST localhost:8000/api/logout \
      -H "Accept: application/json" \
      -H "Content-type: application/json" \
      -H "Authorization: Bearer TOKEN_API"


## Limitaciones

Actualmente la implementacion de la API no cuenta con los siguientes servicios:

- Creacion de nuevos medicamentos
- Creacion de nuevos ingredientes
- Cambio en los ingredientes que un medicamento contenga

