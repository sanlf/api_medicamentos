<?php

namespace Tests\Feature;

use App\User;
use App\Medicamento;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MedicamentoTest extends TestCase
{
    public function testsMedicamentosAreCreatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $payload = [
            'partida'        => '12345',
            'codigo_interno' => '67890',
            'descripcion'    => 'Este es un med',
        ];

        $this->json('POST', '/api/medicamentos', $payload, $headers)
            ->assertStatus(201)
            ->assertJsonFragment([
                'partida'        => '12345',
                'codigo_interno' => '67890',
                'descripcion'    => 'Este es un med',
            ]
        );
    }

    public function testsMedicamentosAreUpdatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $medicamento = factory(Medicamento::class)->create([
            'partida'        => '12345',
            'codigo_interno' => '67890',
            'descripcion'    => 'Este es un med',
        ]);

        $payload = [
            'partida'        => 'xxxxx',
            'codigo_interno' => 'yyyyy',
            'descripcion'    => 'WOWOWOWOWO',
        ];

        $response = $this->json('PUT', '/api/medicamentos/' . $medicamento->id, $payload, $headers)
            ->assertStatus(200)
            ->assertJsonFragment([
                'partida'        => 'xxxxx',
                'codigo_interno' => 'yyyyy',
                'descripcion'    => 'WOWOWOWOWO',
            ]);
    }

    public function testsArtilcesAreDeletedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $medicamento = factory(Medicamento::class)->create([
            'partida'        => '12345',
            'codigo_interno' => '67890',
            'descripcion'    => 'Este es un med',
        ]);

        $this->json('DELETE', '/api/medicamentos/' . $medicamento->id, [], $headers)
            ->assertStatus(204);
    }

    public function testMedicamentosAreListedCorrectly()
    {
        factory(Medicamento::class)->create([
            'partida'        => '12345',
            'codigo_interno' => '67890',
            'descripcion'    => 'Este es un med',
        ]);

        factory(Medicamento::class)->create([
            'partida'        => 'xxxxx',
            'codigo_interno' => 'yyyyy',
            'descripcion'    => 'WOWOWOWOWO',
        ]);

        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/medicamentos', [], $headers)
            ->assertStatus(200)
            ->assertJsonFragment([
                'partida'        => '12345',
                'codigo_interno' => '67890',
                'descripcion'    => 'Este es un med',

                'partida'        => 'xxxxx',
                'codigo_interno' => 'yyyyy',
                'descripcion'    => 'WOWOWOWOWO',

            ])
            ->assertJsonStructure([
                '*' => ['id', 'partida', 'codigo_interno', 'descripcion'],
            ]);
    }
}
