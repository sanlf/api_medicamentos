<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Medicamentos</title>

         <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet" />

        <!-- Styles -->
        <style>
            th, td{
                text-align: center;
                vertical-align: middle;
            }
        </style>
    </head>

    <body>
        <div class="container">
            <h1 align="center">Prueba de API de medicamentos</h1>
        </div>

        <div class="container">
            <table id="tabla_medicamentos" class="table table-bordered table-hover" align="center">
                <thead class="thead-inverse">
                <tr>
                    <th>ID</th>
                    <th>Partida</th>
                    <th>Codigo Interno</th>
                    <th>Descripcion</th>
                    <th>Ingrediente</th>
                    <th>Cantidad</th>
                    <th>Cas</th>
                </tr>
                </thead>

                <tbody>
                    @foreach($medicamentos as $medicamento)
                        <tr>
                            <td rowspan={{$medicamento->ingredientes->count()}}>
                                {{$medicamento->id}}
                            </td>
                            <td rowspan={{$medicamento->ingredientes->count()}}>
                                {{$medicamento->partida}}
                            </td>
                            <td rowspan={{$medicamento->ingredientes->count()}}>
                                {{$medicamento->codigo_interno}}
                            </td>
                            <td rowspan={{$medicamento->ingredientes->count()}}>
                                {{$medicamento->descripcion}}
                            </td>

                            @php $counter = 0; @endphp
                            @if(empty($medicamento->ingredientes))
                                <tr>
                                </tr>
                            @endif
                            @foreach($medicamento->ingredientes as $ingrediente)
                                @if($counter++ == 1)
                                    <tr>
                                @endif
                                    <td>{{$ingrediente->nombre}}</td>
                                    <td class='cantidad'>{{$ingrediente->cantidad}}</td>
                                    <td>{{$ingrediente->cas}}</td>
                                @if($counter++ == 1)
                                    </tr>
                                @endif
                            </tr>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$medicamentos->links()}}
        </div>
    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script>
        $(document).ready(function(){
            $('#tabla_medicamentos td.cantidad').each(function(){
                if($(this).text() === ""){
                    $(this).css('background-color','gray');
                }else if($(this).text() > 50){
                    $(this).css('background-color','red');
                }else{
                    $(this).css('background-color','green');
                }
            });
        });
    </script>
</html>

